# dokos-php

[![pipeline status](https://gitlab.cylab.be/cylab/dokos-php/badges/main/pipeline.svg)](https://gitlab.cylab.be/cylab/dokos-php/-/commits/main)
[![coverage report](https://gitlab.cylab.be/cylab/dokos-php/badges/main/coverage.svg)](https://gitlab.cylab.be/cylab/dokos-php/-/commits/main)

A simple password spraying library in PHP.

## Usage

### Library

Installation:

```bash
composer require cylab/dokos-php
```

Usage:

```php
use Cylab\Dokos\Config;
use Cylab\Dokos\Dokos;


$config = new Config("https://brutus.play.cylab.be");
$config->setLogin("jane.doe@example.com");

$dokos = new Dokos($config);
$result = $dokos->run();

var_dump($result->found);
```

By default the library does not print anything. You can also provide a `Psr\Log\LoggerInterface` instance as second argument, to get some feedback:

```php
use Monolog\Logger;
use Monolog\Handler\StreamHandler;

$logger = new Logger("dokos");
$logger->pushHandler(new StreamHandler("php://stdout"));

$dokos = new Dokos($config, $logger);
```

### CLI

```bash
git clone https://gitlab.cylab.be/cylab/dokos-php.git
cd dokos-php
composer install

php dokos.php --url https://brutus.play.cylab.be --passwords src/1000-password-list.txt --threads 10 --login_field email --login jane.doe@example.com --password_field password --failed "Bad combination of e-mail and password!"
```