<?php

use PHPUnit\Framework\TestCase;

use Cylab\Dokos\Config;
use Cylab\Dokos\Dokos;

class DokosTest extends TestCase
{
    public function testRun(): void
    {
        $config = (new Config("https://brutus.play.cylab.be"))
                ->setLogin("jane.doe@example.com");

        $dokos = new Dokos($config);
        $result = $dokos->run();
        
        $this->assertEquals(1, count($result->found));
        $this->assertEquals(1000, $result->trials);
    }
}