<?php

namespace Cylab\Dokos;

use Garden\Cli\Args;

/**
 * Description of Config
 *
 * @author tibo
 */
class Config
{
    private $url;
    private $login;
    
    private $login_field = "email";
    private $password_field = "password";
    private $threads = 5;
    private $passwords_file = __DIR__ . "/1000-password-list.txt";
    private $failed_message = "Bad combination of e-mail and password!";
    
    
    public function __construct(string $url)
    {
        $this->setUrl($url);
    }
    
    public static function fromGardenCli(Args $args)
    {
        $config = new Config($args->getOpt("url"));
        
        $config->setLogin($args->getOpt("login"))
                ->setPasswordsFile($args->getOpt("passwords-file", $config->getPasswordsFile()))
                ->setThreads($args->getOpt("threads", $config->getThreads()))
                ->setFailedMessage($args->getOpt("failed-message", $config->getFailedMessage()))
                ->setLoginField($args->getOpt("login-field", $config->getLoginField()))
                ->setPasswordField($args->getOpt("password-field", $config->getPasswordField()));
        
        return $config;
    }
    
    public function getUrl()
    {
        return $this->url;
    }

    public function getLogin()
    {
        return $this->login;
    }

    public function getLoginField()
    {
        return $this->login_field;
    }

    public function getPasswordField()
    {
        return $this->password_field;
    }

    public function getThreads()
    {
        return $this->threads;
    }

    public function getPasswordsFile()
    {
        return $this->passwords_file;
    }

    public function getFailedMessage()
    {
        return $this->failed_message;
    }

    public function setUrl(string $url)
    {
        $this->url = $url;
        return $this;
    }

    public function setLogin(string $login)
    {
        $this->login = $login;
        return $this;
    }

    public function setLoginField(string $login_field)
    {
        $this->login_field = $login_field;
        return $this;
    }

    public function setPasswordField(string $password_field)
    {
        $this->password_field = $password_field;
        return $this;
    }

    public function setThreads(int $threads)
    {
        $this->threads = $threads;
        return $this;
    }

    public function setPasswordsFile(string $passwords_file)
    {
        
        if (!is_file($passwords_file)) {
            throw new \InvalidArgumentException($passwords_file . " is not a file!");
        }
        
        $this->passwords_file = $passwords_file;
        return $this;
    }

    public function setFailedMessage(string $failed_message)
    {
        $this->failed_message = $failed_message;
        return $this;
    }
    
    public function stopOnFirst() : bool
    {
        return false;
    }
}
