<?php

namespace Cylab\Dokos;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Pool;
use GuzzleHttp\Psr7\Response;

use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;

/**
 * Description of Dokos
 *
 * @author tibo
 */
class Dokos
{
    // indicates that the thread may continue running
    // used to stop threads if user interrupted processing...
    private $RUN = true;
    
    private $PASSWORDS = [];
    
    private Config $config;
    
    private LoggerInterface $logger;
    
    private Result $result;
    
    public function __construct(Config $config, ?LoggerInterface $logger = null)
    {
        $this->config = $config;
        
        if ($logger == null) {
            $this->logger = new NullLogger();
        } else {
            $this->logger = $logger;
        }
        $this->result = new Result();
    }


    public function showVersion()
    {
        // where is this file located
        $version = trim(file_get_contents(__DIR__ . '/VERSION'));
        $this->logger->info("version: " . $version);
    }

    public function showBanner()
    {
        $this->logger->info("██████╗  ██████╗ ██╗  ██╗ ██████╗ ███████╗      ██████╗ ██╗  ██╗██████╗ ");
        $this->logger->info("██╔══██╗██╔═══██╗██║ ██╔╝██╔═══██╗██╔════╝      ██╔══██╗██║  ██║██╔══██╗");
        $this->logger->info("██║  ██║██║   ██║█████╔╝ ██║   ██║███████╗█████╗██████╔╝███████║██████╔╝");
        $this->logger->info("██║  ██║██║   ██║██╔═██╗ ██║   ██║╚════██║╚════╝██╔═══╝ ██╔══██║██╔═══╝ ");
        $this->logger->info("██████╔╝╚██████╔╝██║  ██╗╚██████╔╝███████║      ██║     ██║  ██║██║     ");
        $this->logger->info("╚═════╝  ╚═════╝ ╚═╝  ╚═╝ ╚═════╝ ╚══════╝      ╚═╝     ╚═╝  ╚═╝╚═╝");

        $this->showVersion();
        $this->logger->info("https://gitlab.cylab.be/cylab/dokos-php");
        $this->logger->info("Use for legal purposes only!");
    }

    public function tryPasswords(iterable $passwords)
    {
        $client = new Client([
            "timeout" => 2.0
        ]);
        
        // https://docs.guzzlephp.org/en/stable/quickstart.html#concurrent-requests
        $requests_generator = function ($passwords) use ($client) {
            foreach ($passwords as $password) {
                $this->result->trials++;
                if (!$this->RUN && $this->config->stopOnFirst()) {
                    break;
                }
                $password = trim($password);
                $this->logger->info("Trying " . $this->config->getLogin() . " and password " . $password . " ...\n");

                $data = [
                    $this->config->getLoginField() => $this->config->getLogin(),
                    $this->config->getPasswordField() => $password
                ];
                
                yield $password => function () use ($client, $data) {
                    return $client->postAsync($this->config->getUrl(), [
                        'form_params' => $data
                    ]);
                };
            }
        };
        
        $pool = new Pool($client, $requests_generator($passwords), [
            'concurrency' => $this->config->getThreads(),
            'fulfilled' => function (Response $response, $password) {
                $body = $response->getBody();

                if (!strpos($body, $this->config->getFailedMessage())) {
                    $this->result->found[] = $password;
                    $this->RUN = false;
                }
            },
            'rejected' => function (RequestException $reason, $password) {
                // handle a failed request
            },
        ]);

        // Initiate the transfers and create a promise
        $promise = $pool->promise();

        // Force the pool of requests to complete.
        $promise->wait();
    }

    public function run() : Result
    {
        $start = new \DateTime();
        
        $this->showBanner();
        $this->readPasswords();

        $this->logger->info('URL: ' . $this->config->getUrl() . "\n");
        $this->logger->info('Login: ' . $this->config->getLogin() . "\n");
        
        $this->tryPasswords($this->PASSWORDS);
        
        $this->logger->info("Done!\n");
        $end = new \DateTime();
        $delta_t = $end->getTimestamp() - $start->getTimestamp();
        $rate = $this->result->trials / $delta_t;
        $this->logger->info("Time: " . $delta_t . " seconds [" . round($rate, 2) . " passwords/sec]\n");
        $this->logger->info("Found " . count($this->result->found) . " password(s): " .
                implode(', ', $this->result->found) . "\n");
        
        return $this->result;
    }

    public function readPasswords()
    {
        $this->PASSWORDS = file($this->config->getPasswordsFile(), FILE_IGNORE_NEW_LINES);
    }
}
