<?php

namespace Cylab\Dokos;

/**
 * Description of Result
 *
 * @author tibo
 */
class Result
{
    
    /**
     * Found passwords
     * @var array
     */
    public $found = [];
    
    /**
     * Number of tried passwords
     * @var int
     */
    public $trials = 0;
}
