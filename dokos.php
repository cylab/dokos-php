<?php

require_once __DIR__ . "/vendor/autoload.php";

use Cylab\Dokos\Config;
use Cylab\Dokos\Dokos;

use Monolog\Logger;
use Monolog\Handler\StreamHandler;

use Garden\Cli\Cli;

$cli = new Cli();

$cli->description('Dokos-PHP')
    ->opt('login:l', 'Username or e-mail to test.', true)
    ->opt('passwords:P', 'Passwords list.', false)
    ->opt('threads:t', 'Number of threads.')
    ->opt('url:u', 'The name of the database to dump.', true)
    ->opt('failed:f', 'Failed authentication message.', false)
    ->opt('login-field', "Login field.", false)
    ->opt('password-field', "Password field.", false)
    ->opt("stop-on-first", "Stop on first match.", false, 'bool');

// Parse and return cli args.
$args = $cli->parse($argv, true);

$config = Config::fromGardenCli($args);

$logger = new Logger("dokos");
$logger->pushHandler(new StreamHandler("php://stdout"));

$dokos = new Dokos($config, $logger);
$dokos->run();
